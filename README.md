# JavaWeb

#### 介绍
java Web增删改查项目，登录页面使用Filter(过滤器)检查session中是否有登录用户的信息，以此拦截匿名用户访问主页面，主界面以一个简单的带分页数据的表格来显示用户的数据，该表格中的数据可以修改与删除，同时也可以按条件查询数据库中的数据，使用Cookies实现记住我的功能，在文件上传模块中使用bootstrap fileinput 产件实现多文件上传的预览和添加，服务器端使用三种可选方式来上传文件，1.IO流、2.ServletFileUpload、3.SmartUpload。

#### 软件架构
jsp+servlet+filter+session+druid+jdbcTemplate+jstl+el表达式+bootstrap4+jquery


#### 安装教程

1. clone或下载该项目，后在运行项目中的数据库文件，建立数据库数据表

2. idea 打开该项目，配置项目运行的Tomcat，修改项目部署时的虚拟路径为空，即什么都不加

3. 浏览器输入localhost:8080端口查看登录页面

4. 数据库文件中的最后两个字段为用户登录时使用的用户名和密码字段，即username和password字段用于登录

5. idea 运行项目有问题的话解决方法如下：

   <img src="images/image-20210728144342975.png" alt="image-20210728144342975" style="zoom:67%;" />

   当web文件将不为idea所识别时解决方法如下

   1. 打开项目文件结构

      ![image-20210728144447726](images/image-20210728144447726.png)

   2. 选择并设置

      ![image-20210728144601822](images/image-20210728144601822.png)

   3. 选择web

      ![image-20210728144625468](images/image-20210728144625468.png)

   4. 选择当前的项目

      ![image-20210728144656353](images/image-20210728144656353.png)

   5. 创建Artifact

      ![image-20210728144729388](images/image-20210728144729388.png)

   6. 点击ok

      ![image-20210728144758035](images/image-20210728144758035.png)

   7. 设置Tomcat

      ![image-20210728144925104](images/image-20210728144925104.png)

      

#### 项目截图

1. 登录界面

   <img src="images/image-20210814211010268.png" alt="image-20210814211010268" style="zoom:67%;" />

2. 登录后界面

   <img src="images/image-20210814211058770.png" alt="image-20210814211058770" style="zoom:67%;" />

3. 主界面

   <img src="images/image-20210814211113360.png" alt="image-20210814211113360" style="zoom:67%;" />

4. 添加界面

   <img src="images/image-20210814211123906.png" alt="image-20210814211123906" style="zoom:67%;" />

5. 修改界面

   <img src="images/image-20210814211134442.png" alt="image-20210814211134442" style="zoom:67%;" />

6. 条件查询与分页界面

   <img src="images/image-20210814211207864.png" alt="image-20210814211207864" style="zoom:67%;" />

7. 删除界面

   <img src="images/image-20210814211227785.png" alt="image-20210814211227785" style="zoom:67%;" />

8. 文件上传界面

   <img src="images/image-20210814211346033.png" alt="image-20210814211346033" style="zoom:67%;" />

9. 文件下载界面

   <img src="images/image-20210814211413034.png" alt="image-20210814211413034" style="zoom:67%;" />

10. 注销

   <img src="images/image-20210814211437200.png" alt="image-20210814211437200" style="zoom:67%;" />











