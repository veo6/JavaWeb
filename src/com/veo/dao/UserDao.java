package com.veo.dao;

import com.veo.entity.User;

import java.util.List;
import java.util.Map;

public interface UserDao {
    public List<User> findAll();
    public User login(String username,String password);
    public void addUser(User user);
    public void deleteUser(String id);
    public User findUserById(int id);
    public int findTotalCount(Map<String, String[]> condition);
    List<User> findByPage(int start, int rows, Map<String, String[]> condition);
}
