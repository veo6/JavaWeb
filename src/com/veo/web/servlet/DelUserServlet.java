package com.veo.web.servlet;

import com.veo.service.UserService;
import com.veo.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/delUserServlet")
public class DelUserServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取ID
        String id = request.getParameter("id");
        //调用Service方法删除
        UserService service=new UserServiceImpl();
        service.deleteUser(id);
        //跳转到UserListServlet（无数据交互）
        response.sendRedirect(request.getContextPath()+"/findUserByPageServlet?currentPage=1&rows=5");


    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);

    }
}
