package com.veo.web.servlet;

import com.veo.entity.User;
import com.veo.service.UserService;
import com.veo.service.impl.UserServiceImpl;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

@WebServlet("/loginServlet")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //设置编码
        request.setCharacterEncoding("UTF-8");

        //获取数据
        //1获取验证码
        String code=request.getParameter("code");
        //验证验证码是否真确
        HttpSession session = request.getSession();
        String checkCode_session =(String) session.getAttribute("checkCode_session");
        //销毁验证码使其唯一
        session.removeAttribute("checkCode_session");
        if(!code.equalsIgnoreCase(checkCode_session)){
            //验证码不正确
            request.setAttribute("login_megs","验证码错误");
            request.getRequestDispatcher("/login.jsp").forward(request,response);
            return;
        }

        Map<String, String[]> map = request.getParameterMap();

        //封装User对象
        User user=new User();
        try {
            BeanUtils.populate(user,map);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //调用Service查询
        UserService service=new UserServiceImpl();
        User loginUser=service.login(user);
        //判断登录是否成功
        if(loginUser!=null){
            //登录成功
            //存入session
            session.setAttribute("user",loginUser);
            //判断用户是否选择了记住我
            String flag = request.getParameter("rememberme");
            System.out.println(flag);
            //记住我的话就将用户信息放到cookie中持久化保存
            if ("on".equals(flag)){
                Cookie rememberUser = new Cookie("rememberUser", loginUser.getUsername() + "&" + loginUser.getPassword() + "&" + "true");
                rememberUser.setMaxAge(60 * 60 * 24);
                response.addCookie(rememberUser);
            }else {
                //清空cookie
                Cookie[] cookies = request.getCookies();
                try {
                    for (Cookie cookie : cookies) {
                        if ("rememberUser".equals(cookie.getName())){
                            Cookie clearCookie = new Cookie(cookie.getName(), null);
                            cookie.setMaxAge(0);
                            response.addCookie(clearCookie);
                        }
                    }
                }catch (Exception e){
                    System.out.println("清空Cookie异常");
                }
            }
            //跳转页面
            response.sendRedirect(request.getContextPath()+"/index.jsp");
        }else {
            //登录失败
            request.setAttribute("login_megs","用户名或密码错误");
            request.getRequestDispatcher("/login.jsp").forward(request,response);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);

    }
}
