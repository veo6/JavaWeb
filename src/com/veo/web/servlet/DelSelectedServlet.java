package com.veo.web.servlet;

import com.veo.service.UserService;
import com.veo.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/delSelectedServlet")
public class DelSelectedServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取id数组
        String[] ids = request.getParameterValues("uid");
        //调用Service方法删除id
        UserService service=new UserServiceImpl();
        service.delSelectedUser(ids);
        //跳转到UserListServlet
        response.sendRedirect(request.getContextPath()+"/findUserByPageServlet?currentPage=1&rows=5");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);

    }
}
