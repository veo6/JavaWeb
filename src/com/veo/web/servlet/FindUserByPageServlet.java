package com.veo.web.servlet;

import com.veo.entity.PageBean;
import com.veo.entity.User;
import com.veo.service.UserService;
import com.veo.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet("/findUserByPageServlet")
public class FindUserByPageServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //编码获取表单提交数据
        request.setCharacterEncoding("UTF-8");

        //1.获取参数
        String currentPage = request.getParameter("currentPage");//当前页码
        String rows = request.getParameter("rows");//获取每页显示记录数
        //判断currentPage和rows的值(index.jep页面的处理)
        if(currentPage == null|| "".equalsIgnoreCase(currentPage)){
            currentPage="1";
        }
        if(rows== null || "".equalsIgnoreCase(rows)){
            rows="5";
        }

        //获取表单提交的参数(查询条件)
        Map<String, String[]> condition = request.getParameterMap();

        //2.调用Service方法
        UserService service=new UserServiceImpl();
        PageBean<User> pb=service.findUserbyPage(currentPage,rows,condition);
        //将PageBean存入request中
        request.setAttribute("pb",pb);
        //回显客户输入的数据
        request.setAttribute("condition",condition);
        //转发到list.jsp页面
        request.getRequestDispatcher("/list.jsp").forward(request,response);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);

    }
}
