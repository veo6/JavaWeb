package com.veo.service.impl;

import com.veo.dao.impl.UserDaoImpl;
import com.veo.entity.PageBean;
import com.veo.entity.User;
import com.veo.service.UserService;

import java.util.List;
import java.util.Map;

public class UserServiceImpl implements UserService {
    @Override
    public List<User> findAll() {
        //调用Dao完成查询
        return new UserDaoImpl().findAll();
    }

    @Override
    public User login(User user) {
        return  new UserDaoImpl().login(user.getUsername(),user.getPassword());
    }

    @Override
    public void addUser(User user) {
        new UserDaoImpl().addUser(user);
    }

    @Override
    public void deleteUser(String id) {
        new UserDaoImpl().deleteUser(id);
    }

    @Override
    public User findUserById(String id) {
        return new UserDaoImpl().findUserById(Integer.valueOf(id));
    }

    @Override
    public void updateUSer(User user) {
        new UserDaoImpl().updateUser(user);
    }

    @Override
    public void delSelectedUser(String[] ids) {
        if(ids!=null&&ids.length!=0){
            for(String id : ids){
                new UserDaoImpl().deleteUser(id);
            }
        }
    }

    @Override
    public PageBean<User> findUserbyPage(String _currentPage, String _rows, Map<String, String[]> condition) {
        //转换对象数据类型
        int currentPage=Integer.parseInt(_currentPage);
        int rows=Integer.parseInt(_rows);
        //判断currentPage为负数的情况置一
        if(currentPage <= 0){
            currentPage=1;
        }

        //创建空的PageBean对象
        PageBean<User> pb=new PageBean<User>();
        //设置参数
        pb.setCurrentPage(currentPage);
        pb.setRows(rows);

        //调用Dao查询总记录数
        int totalCount=new UserDaoImpl().findTotalCount(condition);
        pb.setTotalCount(totalCount);

        //调用Dao查询每页list集合
        //计算每页索引
        int start=(currentPage-1)*rows;
        List<User> list=new UserDaoImpl().findByPage(start,rows,condition);
        pb.setList(list);
        //计算总页码
        int totalPage=(totalCount % rows ==0) ? totalCount/rows : (totalCount/rows)+1;
        pb.setTotalPage(totalPage);
        return pb;
    }
}
