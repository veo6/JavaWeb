package com.veo.service;

import com.veo.entity.PageBean;
import com.veo.entity.User;

import java.util.List;
import java.util.Map;

/**
 * 用户管理的业务接口
 */
public interface UserService {
    public List<User> findAll();
    public User login(User user);
    public void addUser(User user);
    public void deleteUser(String id);
    public User findUserById(String id);
    public void updateUSer(User user);
    public void delSelectedUser(String[] uids);

    /**
     * 分页查询
     * @param currentPage
     * @param rows
     * @param condition
     * @return
     */
    public PageBean<User> findUserbyPage(String currentPage, String rows, Map<String, String[]> condition);
}
