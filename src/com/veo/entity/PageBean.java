package com.veo.entity;

import java.util.List;

/**
 * 分页对象的封装
 * 加泛型可以扩展为其他对象分页（重用性高）
 */
public class PageBean<T> {
    private static final int PAGE_SIZE = 5;
    private int totalCount;//总记录数
    private int totalPage;//总页码
    private List<T> list;//每一页的数据集
    private int currentPage;//当前页码
    private int rows = PAGE_SIZE;//每页的记录总数

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int tatalPage) {
        this.totalPage = tatalPage;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    @Override
    public String toString() {
        return "PageBean{" +
                "totalCount=" + totalCount +
                ", totalPage=" + totalPage +
                ", list=" + list +
                ", currentPage=" + currentPage +
                ", rows=" + rows +
                '}';
    }
}
