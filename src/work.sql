SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `gender` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `age` int(11) NULL DEFAULT NULL,
  `address` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `qq` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '李玉', '女', 18, '四川', '666666', 'liyu@qq.com', 'admin', 'admin');
INSERT INTO `user` VALUES (2, '李峰', '男', 20, '上海', '154156', 'ldsg@qq.com', NULL, NULL);
INSERT INTO `user` VALUES (3, '文书', '女', 19, '广东', '66516444', 'sdfsa@qq.com', NULL, NULL);
INSERT INTO `user` VALUES (4, '文华', '男', 17, '北京', '6464664', 'asfasf@qq.com', NULL, NULL);
INSERT INTO `user` VALUES (5, '罗兰', '女', 22, '重庆', '95464454', 'asfa@qq.com', NULL, NULL);
INSERT INTO `user` VALUES (6, '张三', '男', 18, '广东', '123456', '164341644@qq.com', NULL, NULL);
INSERT INTO `user` VALUES (7, 'boom', '女', 28, '重庆', '1235895223', '121364848@qq.com', NULL, NULL);
INSERT INTO `user` VALUES (8, '李四', '男', 20, '海南', '1235895223', '164341644@qq.com', NULL, NULL);
INSERT INTO `user` VALUES (9, '天天', '男', 23, '新疆', '989651656', '456496@qq.com', NULL, NULL);
INSERT INTO `user` VALUES (10, 'veo', '男', 23, '北京', '123456', '123465@qq.com', NULL, NULL);
INSERT INTO `user` VALUES (11, '王胜', '男', 23, '广东', '123456', '7894646@qq.com', NULL, NULL);
INSERT INTO `user` VALUES (12, '宋妍', '女', 23, '上海', '98959659', '989995659@qq.com', NULL, NULL);
INSERT INTO `user` VALUES (13, '高瑶', '女', 18, '上海', '632662626', '236665@qq.com', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
