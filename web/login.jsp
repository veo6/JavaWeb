<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>登录页面</title>

    <!-- 1. 导入CSS的全局样式 -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- 2. jQuery导入，建议使用1.9以上的版本 -->
    <script src="js/jquery-3.6.0.min.js"></script>
    <!-- 3. 导入bootstrap的js文件 -->
    <script src="js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript">
        function refreshCode (){
            document.getElementById("checkCode").onclick=function () {
                this.src="${pageContext.request.contextPath}/checkCodeServlet?time="+new Date().getTime();
            }
        };
    </script>
    <style>
        a:hover {
            text-decoration: none;
        }
    </style>
</head>
<body>

<%
    String remUserName = "";
    String remUserPwd = "";
    String remStatus = "false";
    Cookie[] cookies = request.getCookies();
    if (cookies.length > 0){
        for (Cookie cookie : cookies) {
            if ("rememberUser".equals(cookie.getName()) && cookie.getValue().length() > 0){
                remUserName = cookie.getValue().split("&")[0];
                remUserPwd = cookie.getValue().split("&")[1];
                remStatus = cookie.getValue().split("&")[2];
                request.setAttribute("remUserName",remUserName);
                request.setAttribute("remUserPwd",remUserPwd);
                request.setAttribute("remStatus",remStatus);
            }
        }
        request.setAttribute("remStatus",remStatus);
    }else {
        request.setAttribute("remStatus",remStatus);
    }
%>
<%--设置国际化信息--%>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="i18n"/>
<div class="container" style="width: 400px;">
    <h3 style="text-align: center;"><fmt:message key="loginTitle"/></h3>

    <nav aria-label="breadcrumb" >
        <ol class="breadcrumb">
            <li class="breadcrumb-item" style="margin: 0 auto"><a href="${pageContext.request.contextPath}/i18n?language=zh_CN"><fmt:message key="language_zh"/></a></li>
            <li class="breadcrumb-item" style="margin: 0 auto"><a href="${pageContext.request.contextPath}/i18n?language=en_US"><fmt:message key="language_en"/></a></li>
        </ol>
    </nav>

    <form action="${pageContext.request.contextPath}/loginServlet" method="post" style="margin-top: 30px">
        <div class="form-group">
            <label for="username"><fmt:message key="username"/>：</label>
            <input type="text" name="username" value="${remUserName}" class="form-control" id="username" placeholder="<fmt:message key="username_pl"/>"/>
        </div>

        <div class="form-group">
            <label for="password"><fmt:message key="password"/>：</label>
            <input type="password" name="password" value="${remUserPwd}" class="form-control" id="password" placeholder="<fmt:message key="password_pl"/>"/>
        </div>

        <div class="form-inline">
            <label for="code"><fmt:message key="code"/>：</label>
            <input type="text" name="code" class="form-control" id="code" placeholder="<fmt:message key="code_pl"/>" style="width: 130px;"/>
            <a href="javascript:refreshCode()" style="margin-left: 30px">
                <img src="${pageContext.request.contextPath}/checkCodeServlet" title="<fmt:message key="code_msg"/>" id="checkCode"/>
            </a>
        </div>
        <div class="form-group form-check" style="margin-top: 5px">
            <c:choose>
                <c:when test="${remStatus == 'true'}">
                    <input type="checkbox" class="form-check-input" name="rememberme" value="on" checked id="rememberme">
                    <label class="form-check-label" for="rememberme"><fmt:message key="rememberme"/></label>
                </c:when>
                <c:otherwise>
                    <input type="checkbox" class="form-check-input" name="rememberme" id="rememberme">
                    <label class="form-check-label" for="rememberme"><fmt:message key="rememberme"/></label>
                </c:otherwise>
            </c:choose>

        </div>
        <hr/>
        <div class="form-group" style="text-align: center;">
            <input class="btn btn btn-primary form-control" type="submit" value="<fmt:message key="login"/>">
        </div>
    </form>

    <!-- 出错显示的信息框 -->
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>${login_megs}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
</body>
</html>
