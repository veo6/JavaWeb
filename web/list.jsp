<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!-- 网页使用的语言 -->
<html lang="zh-CN">
<head>
    <!-- 指定字符集 -->
    <meta charset="utf-8">
    <!-- 使用Edge最新的浏览器的渲染方式 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- viewport视口：网页可以根据设置的宽度自动进行适配，在浏览器的内部虚拟一个容器，容器的宽度与设备的宽度相同。
    width: 默认宽度与设备的宽度相同
    initial-scale: 初始的缩放比，为1:1 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>用户信息管理系统</title>

    <!-- 1. 导入CSS的全局样式 -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap-icons.css">
    <link rel="stylesheet" href="css/fileinput.min.css">
    <!-- 2. jQuery导入，建议使用1.9以上的版本 -->
    <script src="js/jquery-3.6.0.min.js"></script>
    <!-- 3. 导入bootstrap的js文件 -->
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/fileinput.min.js"></script>
    <script src="js/zh.js"></script>
    <style type="text/css">
        td, th {
            text-align: center;
        }
    </style>
    <script>
        function deleteUser(id) {
            if(confirm("确定要删除吗？")){
                location.href="${pageContext.request.contextPath}/delUserServlet?id="+id;
            }
        }
        window.onload=function () {
            document.getElementById("delSelected").onclick = function () {
                //完成表单的提交
                if(confirm("确定删除选中吗？")){
                    //判断是否有选中的信息（不判断出现空指针异常，并弹出确认框）
                    var flag=false;
                    var cbs=document.getElementsByName("uid");
                    for(var i=0;i<cbs.length;i++){
                        //判断是否选中
                        if(cbs[i].checked){
                            flag=true;
                            break;
                        }
                    }
                    if(flag==true){
                        document.getElementById("form").submit();
                    }
                }else {
                    return;
                }
            }
            document.getElementById("firstCb").onclick=function () {
                var cbs=document.getElementsByName("uid");
                for(var i=0;i<cbs.length;i++){
                    //实现全选
                    cbs[i].checked=this.checked;//与当前对象的状态一样
                }

            }
        }
    </script>
</head>
<body>
<div class="container-fluid" style="width: 1400px">
    <h3 style="text-align: center">用户信息列表</h3>
    <div style="float: left">
        <form class="form-inline" action="${pageContext.request.contextPath}/findUserByPageServlet" method="post">
            <div class="form-group">
                <label for="exampleInputName2">姓名</label>
                <input type="text" name="name" value="${condition.name[0]}" class="form-control" id="exampleInputName2">
            </div>
            <div class="form-group">
                <label for="exampleInputAddress2">籍贯</label>
                <input type="text" name="address" value="${condition.address[0]}" class="form-control" id="exampleInputAddress2">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail2">邮箱</label>
                <input type="email" name="email" value="${condition.email[0]}" class="form-control" id="exampleInputEmail2">
            </div>
            &nbsp;&nbsp;
            <button type="submit" class="btn btn-outline-primary"><i class="bi bi-search"></i>查询</button>
        </form>
    </div>
    <div style="float: right; margin: 5px;  ">
        <a class="btn btn-outline-info" href="${pageContext.request.contextPath}/add.jsp"><i class="bi bi-plus"></i>添加</a>
        <a class="btn btn-outline-danger" href="javascript:void(0)" id="delSelected"><i class="bi bi-person-x"></i>删除选中</a>
        <div class="btn-group" role="group">
            <button id="btnGroupDrop1" type="button" class="btn btn-outline-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <i class="bi bi-nut"></i>
                更多
            </button>
            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                <a class="dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target="#fileModal"><i class="bi bi-cloud-arrow-up"></i>文件上传</a>
                <a class="dropdown-item" href="${pageContext.request.contextPath}/fileDown.jsp"><i class="bi bi-cloud-arrow-down"></i></i>文件下载</a>
                <a class="dropdown-item" href="${pageContext.request.contextPath}/logoutServlet"><i class="bi bi-arrow-bar-right"></i></i>注销</a>
            </div>
        </div>
    </div>
    <form action="${pageContext.request.contextPath}/delSelectedServlet" id="form" method="post">
    <table border="1" class="table table-responsive-sm table-bordered table-hover">
        <thead class="thead-light">
            <tr>
                <th><input type="checkbox" id="firstCb"></th>
                <th>编号</th>
                <th>姓名</th>
                <th>性别</th>
                <th>年龄</th>
                <th>籍贯</th>
                <th>QQ</th>
                <th>邮箱</th>
                <th>操作</th>
            </tr>
        </thead>

        <tbody>
            <c:forEach var="user" items="${pb.list}" varStatus="s">
                <tr>
                    <!--获取表单提交的id-->
                    <td><input type="checkbox" name="uid" value="${user.id}"></td>
                    <td>${s.count}</td>
                    <td>${user.name}</td>
                    <td>${user.gender}</td>
                    <td>${user.age}</td>
                    <td>${user.address}</td>
                    <td>${user.qq}</td>
                    <td>${user.email}</td>
                    <td>
                        <a class="btn btn-light btn-sm" href="${pageContext.request.contextPath}/findUserServlet?id=${user.id}"><i class="bi bi-vector-pen"></i>修改</a>&nbsp;
                        <a class="btn btn-danger btn-sm" href="javascript:deleteUser(${user.id})"><i class="bi bi-trash"></i>删除</a>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    </form>
    <div>
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                
                <c:choose>
                    <%--页数小于5页就直接显示所有的页数--%>
                    <c:when test="${pb.totalPage <= 5}">
                        <c:set var="begin" value="1"/>
                        <c:set var="end" value="${pb.totalPage}"/>
                    </c:when>
                    <%--大于5也的话在分情况--%>
                    <c:when test="${pb.totalPage > 5}">
                        <c:choose>
                            <%--当前页码为前面 3 个：1，2，3 的情况，页码范围是：1-5.--%>
                            <c:when test="${pb.currentPage <= 3}">
                                <c:set var="begin" value="1"/>
                                <c:set var="end" value="5"/>
                            </c:when>

                            <%--当前页码为最后 3 个，8，9，10，页码范围是：总页码减 4 - 总页码--%>
                            <c:when test="${pb.currentPage > pb.totalPage - 3}">
                                <c:set var="begin" value="${pb.totalPage - 4}"/>
                                <c:set var="end" value="${pb.totalPage}"/>
                            </c:when>

                            <%--当前页码：4，5，6，7，页码范围是：当前页码减 2 - 当前页码加 2--%>
                            <c:otherwise>
                                <c:set var="begin" value="${pb.currentPage - 2}"/>
                                <c:set var="end" value="${pb.currentPage + 2}"/>
                            </c:otherwise>
                        </c:choose>
                    </c:when>
                </c:choose>

                <!--页面上下的跳转（第一页要判断）-->
                <c:if test="${pb.currentPage == 1}">
                <li class="page-item disabled">
                </c:if>

                <c:if test="${pb.currentPage != 1}">
                <li class="page-item">
                    <a class="page-link" href="${pageContext.request.contextPath}/findUserByPageServlet?currentPage=1&name=${condition.name[0]}&address=${condition.address[0]}&email=${condition.email[0]}" aria-label="Previous">
                        <span aria-hidden="true">首页</span>
                    </a>
<%--                    <a href="${pageContext.request.contextPath}/findUserByPageServlet?currentPage=1&name=${condition.name[0]}&address=${condition.address[0]}&email=${condition.email[0]}" aria-label="Previous">--%>
<%--                        <span aria-hidden="true">首页</span>--%>
<%--                    </a>--%>
                </li>

                <li>
                </c:if>
                    <a class="page-link" href="${pageContext.request.contextPath}/findUserByPageServlet?currentPage=${pb.currentPage-1}&name=${condition.name[0]}&address=${condition.address[0]}&email=${condition.email[0]}" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>

<%--                    <a href="${pageContext.request.contextPath}/findUserByPageServlet?currentPage=${pb.currentPage-1}&name=${condition.name[0]}&address=${condition.address[0]}&email=${condition.email[0]}" aria-label="Previous">--%>
<%--                        <span aria-hidden="true">&laquo;</span>--%>
<%--                    </a>--%>
                </li>
                <!--foreach循环获取页码-->
                <c:forEach begin="${begin}" end="${end}" var="i">
                    <c:if test="${pb.currentPage == i}">
                        <li class="page-item active"><a class="page-link" href="${pageContext.request.contextPath}/findUserByPageServlet?currentPage=${i}&rows=${pb.rows}&name=${condition.name[0]}&address=${condition.address[0]}&email=${condition.email[0]}">${i}</a></li>
                    </c:if>
                    <c:if test="${pb.currentPage != i}">
                        <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/findUserByPageServlet?currentPage=${i}&rows=${pb.rows}&name=${condition.name[0]}&address=${condition.address[0]}&email=${condition.email[0]}">${i}</a></li>
                    </c:if>
                </c:forEach>
                <!--判断最后的页码值-->
                <c:if test="${pb.currentPage == pb.totalPage}">
                <li class="page-item disabled">
                </c:if>

                 <c:if test="${pb.currentPage != pb.totalPage}">
                    <li class="page-item">
                        <a class="page-link" href="${pageContext.request.contextPath}/findUserByPageServlet?currentPage=${pb.totalPage}&name=${condition.name[0]}&address=${condition.address[0]}&email=${condition.email[0]}" aria-label="Previous">
                            <span aria-hidden="true">尾页</span>
                        </a>
                    </li>
                 <li>
                 </c:if>
                    <a class="page-link" href="${pageContext.request.contextPath}/findUserByPageServlet?currentPage=${pb.currentPage==pb.totalPage ? pb.totalPage : pb.currentPage+1}&rows=${pb.rows}&name=${condition.name[0]}&address=${condition.address[0]}&email=${condition.email[0]}" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
                <!--获取页码与记录数-->
                <span style="margin-left: 10px; font-size: 25px">
                    当前第${pb.currentPage}页，共${pb.totalCount}条数据，共${pb.totalPage}页
                </span>
            </ul>
        </nav>
    </div>
</div>

<!-- 文件上传模态框 -->
<div class="modal fade" id="fileModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="fileModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="fileModalLabel">文件上传</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="${pageContext.request.contextPath}/fileUploadAndDownServlet" id="fileUploadForm" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="name" class="control-label">文件名:</label>
                        <input type="text" class="form-control" id="name" name="name" required>
                    </div>
                    <div class="form-group">
                        <label for="file" class="control-label">文件:</label>
                        <input type="file" id="file" class="file" multiple name="uploadFile" required/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<%--<div class="modal fade" id="fileModal" tabindex="-1" role="dialog" aria-labelledby="fileModalLabel">--%>
<%--    <div class="modal-dialog" role="document">--%>
<%--        <div class="modal-content">--%>
<%--            <div class="modal-header">--%>
<%--                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>--%>
<%--                <h4 class="modal-title" id="fileModalLabel">文件上传</h4>--%>
<%--            </div>--%>
<%--            <div class="modal-body">--%>
<%--                <form action="${pageContext.request.contextPath}/fileUploadAndDownServlet" id="fileUploadForm" method="post" enctype="multipart/form-data">--%>
<%--                    <div class="form-group">--%>
<%--                        <label for="name" class="control-label">文件名:</label>--%>
<%--                        <input type="text" class="form-control" id="name" name="name" required>--%>
<%--                    </div>--%>
<%--                    <div class="form-group">--%>
<%--                        <label for="file" class="control-label">文件:</label>--%>
<%--                        <input type="file" id="file" class="file" multiple name="uploadFile" required/>--%>
<%--                    </div>--%>
<%--                </form>--%>
<%--            </div>--%>
<%--            <div class="modal-footer">--%>
<%--                <button type="button" id="uploadBtn" class="btn btn-primary">上传</button>--%>
<%--                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--</div>--%>

<script>
    $.fn.fileinputBsVersion = '3.3.7';
    $('#file').fileinput({
        language: 'zh',     //设置语言
        dropZoneEnabled: true,      //是否显示拖拽区域
        dropZoneTitle: "可以将文件拖放到这里",    //拖拽区域显示文字
        uploadExtraData: function () {
            var name = $('#name').val();
            return {name : name}
        },
        uploadUrl: '${pageContext.request.contextPath}/fileUploadAndDownServlet',  //上传路径
        method: 'post',
        // allowedFileExtensions: ['jpg', 'png', 'gif', 'jpeg'],   //指定上传文件类型,不填不限制
        maxFileSize: 1048576,   //上传文件最大值，单位kb
        uploadAsync: true,  //异步上传
        maxFileCount: 6,    //上传文件最大个数

    }).on("fileuploaded", function(event,data) { //异步上传成功后回调
        console.log(data);		//data为返回的数据
        console.log(data.response);
        if (data.response === 'ok'){
            if (confirm("文件上传成功，是否前往下载？")){
                $(".kv-file-remove").click();
                $("#name").val("");
                location = '${pageContext.request.contextPath}/' + "fileDown.jsp";
            }else {
                $(".kv-file-remove").click();
                $("#name").val("");
            }
        }else {
            alert("文件删除失败，请稍后再试！")
        }
    });

</script>
</body>
</html>
